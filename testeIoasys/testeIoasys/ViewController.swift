//
//  ViewController.swift
//  testeIoasys
//
//  Created by Lucas De Assis on 06/11/2019.
//  Copyright © 2019 teste. All rights reserved.
//

import UIKit
import Heimdallr
import Alamofire

class ViewController: UIViewController
{
    var authModel = AuthModel()
    
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var senhaTF: UITextField!
    @IBOutlet weak var entratBT: UIButton!
    
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:))))
    }
    @IBAction func loginButton(_ sender: Any)
    {
        if(emailTF.text! != nil && emailTF.text! != "" && emailTF.text!.count > 0)
        {
            Login()
        }
        else
        {
            //email inválido
        }
    }
    
    func Login()
    {
        let user = emailTF.text!
        let password = senhaTF.text!
        
        let credentialData = "\(user):\(password)".data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!
        let base64Credentials = credentialData.base64EncodedString()
        let headers =
            [
                "Authorization": "Basic \(base64Credentials)",
                "Accept": "application/json",
                "Content-Type": "application/json"
        ]
        let params =
            [
                "email": user,
                "password":password
        ]
        Alamofire.request("https://empresas.ioasys.com.br/api/v1/users/auth/sign_in", method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers) .responseJSON
            { response in
                switch response.result
                {
                case .success(let JSON):
                    print(JSON)
                    if let contentType = response.response?.allHeaderFields["Content-Type"] as? String
                    {
                        print(response.response?.allHeaderFields)
                        //getting the access token, uid and client
                        
                        if let accessToken = response.response?.allHeaderFields["access-token"] as? String
                        {
                            // authenticatioon
                            self.authModel.AccessToken = accessToken
                            self.authModel.Client = response.response?.allHeaderFields["client"] as! String
                            self.authModel.Uid = response.response?.allHeaderFields["uid"] as! String
                            //User autenticathed
                            self.performSegue(withIdentifier: "homeSegue", sender: self)
                        }
                        else
                        {
                            //login ou senha inválidos
                            var refreshAlert = UIAlertController(title: "Login não realizado", message: "E-mail ou senha inválidos", preferredStyle: UIAlertController.Style.alert)
                            
                            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler:
                                { (action: UIAlertAction!) in
                                    return
                            }))
                            
                            self.present(refreshAlert, animated: true, completion: nil)
                        }
                        print(contentType)
                    }
                case .failure(_):
                    break
                }
                
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if((segue.destination as? HomeViewController) != nil)
        {
            var vc = segue.destination as! HomeViewController
            vc.authModel = authModel
        }
    }

}

