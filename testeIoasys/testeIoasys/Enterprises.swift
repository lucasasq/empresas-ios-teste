//
//  Enterprises.swift
//  testeIoasys
//
//  Created by Lucas De Assis on 06/11/2019.
//  Copyright © 2019 teste. All rights reserved.
//

import Foundation
class Enterprises
{
    struct EnterpriseJSON: Codable
    {
        var city : String?
        var country : String?
        var description : String?
        var enterprise_name : String?
        var enterprise_type_name : String?
    }
    var city : String?
    var country : String?
    var description : String?
    var enterprise_name : String?
    var enterprise_type_name : String?
}
