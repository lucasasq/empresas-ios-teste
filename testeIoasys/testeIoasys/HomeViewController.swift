//
//  HomeViewController.swift
//  testeIoasys
//
//  Created by Lucas De Assis on 06/11/2019.
//  Copyright © 2019 teste. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class HomeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate
{
    
    
    var authModel = AuthModel()
    var enterpriseTable = [Enterprises]()
    var dataAux = [Enterprises] ()
    var enterpriseObjectChoosed = Enterprises()
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        print("\n\n\n \(authModel.AccessToken!)")
        print("\(authModel.Client!)")
        print("\(authModel.Uid!)")
        
        tableView.delegate = self
        tableView.isHidden = true
        let nib = UINib(nibName: "EnterpriseTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "customCell")
        GetData()
    }
    
    func GetData()
    {
        let user = "testeapple@ioasys.com.br"
        let password = "12341234"
        
        let credentialData = "\(user):\(password)".data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!
        let base64Credentials = credentialData.base64EncodedString()
        
        let headers =
            [
                "Content-Type": "application/json",
                "Access-token": authModel.AccessToken!,
                "Client" : authModel.Client!,
                "Uid" : authModel.Uid!
            ]
        let params =
            [
                "email": user,
                "password":password,
                "Access-token": authModel.AccessToken!,
                "Client" : authModel.Client!,
                "Uid" : authModel.Uid!
            ]
        
        Alamofire.request("https://empresas.ioasys.com.br/api/v1/enterprises", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers)
            .responseJSON
            {
                response in
                switch response.result
                {
                case .success(let json):
                    print(json)
                    do
                    {
                        let JSONTeste = json as! NSDictionary
                        self.GetEnterprises(enterprises: JSONTeste)
                    }catch
                    {
                        print("Error: \(error)")
                    }
                case .failure(let error):
                    print(error)
                }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if(dataAux.count>0)
        {
            return dataAux.count
        }
        else if(dataAux.count == 0 && (searchBar.text?.count)! > 0)
        {
            return dataAux.count
        }
        else
        {
            return enterpriseTable.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        guard let cell : EnterpriseTableViewCell = tableView.dequeueReusableCell(withIdentifier: "customCell") as? EnterpriseTableViewCell else
        {
            print("Dequeued cell isn't an instance of CustomVendasTableCell")
            fatalError()
        }
        
        var enterpriseRow = Enterprises()
        
        if(dataAux.count>0)
        {
            enterpriseRow=dataAux[indexPath.row]
        }
            
        else
        {
            enterpriseRow = enterpriseTable[indexPath.row]
            print(indexPath.row)
        }
        
        cell.nomeEmpresaLabel.text = enterpriseRow.enterprise_name
        cell.ramoLabel.text = enterpriseRow.enterprise_type_name
        cell.paisLabel.text = enterpriseRow.country
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        var enterpriseRow = Enterprises()
        if(dataAux.count>0)
        {
            enterpriseRow=dataAux[indexPath.row]
        }
        else
        {
            enterpriseRow = enterpriseTable[indexPath.row]
            print(indexPath.row)
        }
        
        enterpriseObjectChoosed = enterpriseRow
        self.performSegue(withIdentifier: "detailSegue", sender: self)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar)
    {
        dataAux.removeAll()
        tableView.reloadData()
        self.view.endEditing(true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        self.view.endEditing(true)
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        searchBar.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        let text = searchText.lowercased()
        dataAux.removeAll()
        if(text.count>0)
        {
            for item in enterpriseTable
            {
                if((item.country?.lowercased().contains(text.lowercased()))! || (item.enterprise_name?.lowercased().contains(text.lowercased()))! || (item.enterprise_type_name?.contains(text.lowercased()))!)
                {
                    self.dataAux.append(item)
                }
            }
            tableView.reloadData()
        }
        else
        {
            self.view.endEditing(true)
            dataAux.removeAll()
            tableView.reloadData()
        }
    }
    
    func GetEnterprises(enterprises : NSDictionary)
    {
        let json = JSON(enterprises)
        for (key,subJson):(String, JSON) in json
        {
            for i in 0 ... subJson.array!.count
            {
                var city = subJson[i]["city"]
                var country = subJson[i]["country"]
                var description = subJson[i]["description"]
                var enterprise_name = subJson[i]["enterprise_name"]
                
                var enterprise_type_name = subJson[i]["enterprise_type"]["enterprise_type_name"]
                //print(enterprise_type_name)
                var entepriseObject = Enterprises()
                entepriseObject.city = city.stringValue
                entepriseObject.country = country.stringValue
                entepriseObject.description = description.stringValue
                entepriseObject.enterprise_name = enterprise_name.stringValue
                entepriseObject.enterprise_type_name = enterprise_type_name.stringValue
                
                if(entepriseObject.city != nil && entepriseObject.city != "")
                {
                    self.enterpriseTable.append(entepriseObject)
                }
            }
        }
        
        tableView.reloadData()
        tableView.isHidden = false
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if((segue.destination as? DetailViewController) != nil)
        {
            var vc = segue.destination as! DetailViewController
            vc.enterpriseChoosed = self.enterpriseObjectChoosed            
        }
    }
    
}
