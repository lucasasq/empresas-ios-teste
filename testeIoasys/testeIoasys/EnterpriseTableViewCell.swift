//
//  EnterpriseTableViewCell.swift
//  testeIoasys
//
//  Created by Lucas De Assis on 07/11/2019.
//  Copyright © 2019 teste. All rights reserved.
//

import UIKit

class EnterpriseTableViewCell: UITableViewCell {

    @IBOutlet weak var imageViewEnterprise: UIImageView!
    @IBOutlet weak var nomeEmpresaLabel: UILabel!
    @IBOutlet weak var ramoLabel: UILabel!
    @IBOutlet weak var paisLabel: UILabel!
    override func awakeFromNib()
    {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
