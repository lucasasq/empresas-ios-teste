//
//  DetailViewController.swift
//  testeIoasys
//
//  Created by Lucas De Assis on 07/11/2019.
//  Copyright © 2019 teste. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController
{
    var enterpriseChoosed = Enterprises()
    
    @IBOutlet weak var descriptionTextView: UITextView!
    override func viewDidLoad()
    {
        super.viewDidLoad()
        descriptionTextView.text = enterpriseChoosed.description!
    }
    
}
